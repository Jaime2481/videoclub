@extends('layouts.master')

@section('content')

<form action="{{route('edit', $pelicula->id)}}" method="POST"  style="width: 50%; margin: 0px auto;">
		<input type="hidden" name="_method" value="PUT">
   		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<h2>Modificar "<i><?php echo $pelicula->title; ?></i>"</h2>
		<div class="form-group">
		   <label for="title">Título</label>
		   <input type="text" name="title" id="title" class="form-control" placeholder="Título película" required value="{{$pelicula->title}}">
		 </div>
		 <div class="form-group">
		   <label for="year">Año</label>
		   <input type="number" name="year" id="year" max="2020" class="form-control" placeholder="Año de estreno" required  value="{{$pelicula->year}}">
		 </div>
		 <div class="form-group">
		   <label for="director">Director</label>
		   <input type="text" name="director" id="director" class="form-control" placeholder="Nombre del director" required  value="{{$pelicula->director}}">
		 </div>
		 <div class="form-group">
		   <label for="poster">Poster</label>
		   <input type="url" name="poster" id="poster" class="form-control" placeholder="Pega la url del poster" required  value="{{$pelicula->poster}}">
		 </div>
		 <div class="form-group">
		  	<label for="synopsis">Sinópsis</label>
		  	<textarea name="synopsis" id="synopsis" class="form-control" rows="5" style="resize: none;" required>{{$pelicula->synopsis}}</textarea>
		</div>
		<input type="submit" class="btn btn-success" value="Modificar película">
		<a href="{{url('catalog/show/'.$pelicula->id)}}" class="btn btn-danger">Cancelar</a>
	</form>

@stop

