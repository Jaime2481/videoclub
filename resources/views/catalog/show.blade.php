@extends('layouts.master')

@section('content')

<table class="text-center" style="width: 80%; margin: 0px auto;">
	<tr>
		<td rowspan="3">
			<img src="<?php echo $pelicula->poster; ?>" alt="Poster de <?php echo $pelicula->title; ?>" height="500">
		</td>
		<td>
			<h2><?php echo $pelicula->title." (".$pelicula->year.")"; ?></h2>
		</td>
	</tr>
	<tr>
		<td>
			<p>Dirigida por <strong><?php echo $pelicula->director; ?></strong></p>
		</td>
	</tr>
	<tr>
		<td class="text-left" style="padding: 20px;">
			<p><?php echo $pelicula->synopsis; ?></p>
			<p>
				<a href="{{url('catalog/edit/'.$pelicula->id)}}" class="btn btn-warning mx-3" style="margin: 5px 5px; width: 47%;"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>  Editar película</a>
				<a href="{{url('catalog')}}" class="btn btn-primary mx-3" style="margin: 5px 5px; width: 47%;"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>  Volver al catálogo</a>
			</p>
			<form action="{{url('catalog/alquilar/'.$pelicula->id)}}" method="POST">
				@csrf
				@method('put')
				<?php if ($pelicula->rented==0) { ?>
				<button type="submit" class="btn btn-success center-block" style="width: 50%;"><span class="glyphicon glyphicon-download" aria-hidden="true"></span>  Alquilar</button>
				<?php } else { ?>
				<button type="submit" class="btn btn-danger center-block" style="width: 50%;"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span>  Devolver pelicula</button>
				<?php } ?>
			</form>
			<br/>
			<form action="{{url('catalog/delete/'.$pelicula->id)}}" method="POST">
				@csrf
				@method('delete')
				<button type="submit" class="btn btn-danger mx-3" style="width: 100%;">Eliminar</button>
			</form>
		</td>
	</tr>

</table>
@stop
