<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Create</title>
</head>
<body>
@extends('layouts.master')
@section('content')
 <form action="{{ route('crear') }}" method="POST"  style="width: 50%; margin: 0px auto;" autocomplete="off">
	<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
	<div class="form-group">
	   <label for="title">Título</label>
	   <input type="text" name="title" id="title" class="form-control" placeholder="Título película">
	 </div>
	 <div class="form-group">
	   <label for="year">Año</label>
	   <input type="number" name="year" id="year" max="2020" class="form-control" placeholder="Año de estreno" required>
	 </div>
	 <div class="form-group">
	   <label for="director">Director</label>
	   <input type="text" name="director" id="director" class="form-control" placeholder="Nombre del director" required>
	 </div>
	 <div class="form-group">
	   <label for="poster">Poster</label>
	   <input type="url" name="poster" id="poster" class="form-control" placeholder="Pega la url del poster" required>
	 </div>
	 <div class="form-group">
		  <label for="synopsis">Sinópsis</label>
		  <textarea name="synopsis" id="synopsis" class="form-control" rows="5" style="resize: none;" required></textarea>
	</div>
	<input type="submit" class="btn btn-success" value="Crear película">
</form>


@stop

</body>
</html>
