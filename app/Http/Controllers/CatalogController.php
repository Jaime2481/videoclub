<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Movie;
use App\Models\User;

class CatalogController extends Controller
{
 
    public function getIndex()
    {
        $peliculas = Movie::all();
    	return view('catalog.index', array('arrayPeliculas'=>$peliculas));
    }

    public function getShow($id)
    {
    	$pelicula = Movie::findOrFail($id);
    	return view('catalog.show',array('pelicula'=>$pelicula ));
    }
    public function getCreate()
    {
        return view('catalog.create');
    }
    public function getEdit($id)
    {
        $pelicula = Movie::findOrFail($id);
    	return view('catalog.edit',array('pelicula'=>$pelicula, 'id'=>$id));
    }

    public function postCreate(Request $request) {
        $pelicula = new Movie;
        $pelicula->title = $request->input('title');
        $pelicula->year = $request->input('year');
        $pelicula->director = $request->input('director');
        $pelicula->poster = $request->input('poster');
        $pelicula->synopsis = $request->input('synopsis');
        $pelicula->save();
        return redirect('catalog');
    }

    public function putEdit(Request $request, $id) {
        $pelicula = Movie::find($id);
        $pelicula->title = $request->input('title');
        $pelicula->year = $request->input('year');
        $pelicula->director = $request->input('director');
        $pelicula->poster = $request->input('poster');
        $pelicula->synopsis = $request->input('synopsis');
        $pelicula->save();
        return redirect('catalog/show/'.$id);
    }

    public function getDelete($id) {
        $pelicula = Movie::find($id);
        $pelicula->delete();
        return redirect('catalog');
    }
    public function putAlquilaDevuelve($id) {
        $pelicula = Movie::find($id);
        if ($pelicula->rented==0) {
            $pelicula->rented = 1;
        } else {
            $pelicula->rented = 0;
        }
        $pelicula->save();
        return redirect('catalog/show/'.$id);
    }
    public function años() {
        $peliculas = DB::select('SELECT title, year FROM movies ORDER BY year ASC');
        $max = DB::select('SELECT title FROM movies WHERE year=(SELECT MAX(year) FROM movies)');
        $min = DB::select('SELECT title FROM movies WHERE year=(SELECT MIN(year) FROM movies)');
        return view('catalog.viejaynueva', array('peliculas'=>$peliculas, 'max'=>$max, 'min'=>$min));
    }
}
